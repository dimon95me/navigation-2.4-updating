package com.example.updatedfragmentandnavigation.utils

import androidx.core.net.toUri
import androidx.navigation.NavDeepLinkRequest

class NavigationDeepLinkHelper {
    companion object {
        fun getOrdersRequest(from: String, result: String) = NavDeepLinkRequest.Builder
            .fromUri("testapp://orders/$from/$result".toUri())
            .build()
    }
}