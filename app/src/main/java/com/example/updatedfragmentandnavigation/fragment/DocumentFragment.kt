package com.example.updatedfragmentandnavigation.fragment

import com.example.updatedfragmentandnavigation.base.BaseFragment
import com.example.updatedfragmentandnavigation.databinding.FragmentDocumentBinding

class DocumentFragment : BaseFragment<FragmentDocumentBinding>() {
    override fun getViewBinding() = FragmentDocumentBinding.inflate(layoutInflater)

    override fun getHasOptionsMenu() = false

    override fun getShowBottomNavigation() = true

    override fun initView() {

    }

}