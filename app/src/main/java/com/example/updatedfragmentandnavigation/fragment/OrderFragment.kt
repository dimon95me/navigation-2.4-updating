package com.example.updatedfragmentandnavigation.fragment

import com.example.updatedfragmentandnavigation.R
import com.example.updatedfragmentandnavigation.base.BaseFragment
import com.example.updatedfragmentandnavigation.databinding.FragmentOrderBinding

class OrderFragment : BaseFragment<FragmentOrderBinding>() {
    override fun getViewBinding() = FragmentOrderBinding.inflate(layoutInflater)

    override fun getHasOptionsMenu() = false

    override fun getShowBottomNavigation() = true

    override fun initView() {
        arguments?.let { args ->
            args.getString(ARG_FROM)?.let {
                binding.from.text = getString(R.string.from, it)
            }
            args.getString(ARG_RESULT)?.let {
                binding.result.text = getString(R.string.result, it)
            }
        }
    }

    companion object {
        const val ARG_FROM = "from"
        const val ARG_RESULT = "result"
    }

}