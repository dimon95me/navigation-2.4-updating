package com.example.updatedfragmentandnavigation.fragment

import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.updatedfragmentandnavigation.R
import com.example.updatedfragmentandnavigation.base.BaseFragment
import com.example.updatedfragmentandnavigation.databinding.FragmentCatalogBinding

class CatalogFragment : BaseFragment<FragmentCatalogBinding>() {
    override fun getViewBinding()= FragmentCatalogBinding.inflate(layoutInflater)

    override fun initView() {
        binding.toProduct.setOnClickListener {
            findNavController().navigate(R.id.action_catalogFragment_to_productDetailsFragment, bundleOf(
                ProductDetailsFragment.ARG_CLASS_NAME to this::class.java.simpleName
            ))
        }

        arguments?.getString(ARG_CLASS_NAME)?.let {
            binding.from.text = getString(R.string.from, it)
        }
    }

    companion object {
        const val ARG_CLASS_NAME = "arg_class_name"
    }

}