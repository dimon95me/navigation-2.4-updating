package com.example.updatedfragmentandnavigation.fragment

import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.updatedfragmentandnavigation.R
import com.example.updatedfragmentandnavigation.base.BaseFragment
import com.example.updatedfragmentandnavigation.databinding.FragmentProductDetailsBinding
import com.example.updatedfragmentandnavigation.utils.NavigationDeepLinkHelper


class ProductDetailsFragment : BaseFragment<FragmentProductDetailsBinding>() {

    override fun getViewBinding() = FragmentProductDetailsBinding.inflate(layoutInflater)

    override fun initView() {
        binding.toOrders.setOnClickListener {
            findNavController().popBackStack(R.id.catalogFragment, true)

            requireActivity().findNavController(R.id.main_fragment_nav).navigate(
                NavigationDeepLinkHelper.getOrdersRequest(
                    this::class.java.simpleName,
                    getString(R.string.product_details_result)
                )
            )
        }

        arguments?.getString(CatalogFragment.ARG_CLASS_NAME)?.let {
            binding.from.text = getString(R.string.from, it)
        }
    }

    companion object {
        const val ARG_CLASS_NAME = "arg_class_name"
    }

}