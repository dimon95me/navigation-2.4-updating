package com.example.updatedfragmentandnavigation.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.updatedfragmentandnavigation.R

class EmptyFragment : Fragment(R.layout.fragment_emty)