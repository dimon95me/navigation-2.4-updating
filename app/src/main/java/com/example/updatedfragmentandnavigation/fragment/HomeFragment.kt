package com.example.updatedfragmentandnavigation.fragment

import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.updatedfragmentandnavigation.R
import com.example.updatedfragmentandnavigation.base.BaseFragment
import com.example.updatedfragmentandnavigation.databinding.FragmentHomeBinding


class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    override fun getViewBinding() = FragmentHomeBinding.inflate(layoutInflater)

    override fun getHasOptionsMenu() = false

    override fun getShowBottomNavigation() = true

    override fun initView() {
        binding.apply {
            toCatalog.setOnClickListener {
                findNavController().navigate(
                    R.id.action_homeFragment_to_navigation_catalog,
                    bundleOf(
                        CatalogFragment.ARG_CLASS_NAME to this::class.java.simpleName
                    )
                )
            }

            arrayOf(TAB_ITEM_NEWS, TAB_ITEM_FINANCES).forEach {
                tabLayout.addTab(tabLayout.newTab().setText(it))
            }
        }
    }

    companion object {
        const val TAB_ITEM_NEWS = "tab_item_news"
        const val TAB_ITEM_FINANCES = "tab_item_finances"
    }
}