package com.example.updatedfragmentandnavigation.fragment

import androidx.fragment.app.FragmentActivity
import com.example.updatedfragmentandnavigation.base.BaseFragment
import com.example.updatedfragmentandnavigation.databinding.FragmentSearchBinding
import com.example.updatedfragmentandnavigation.view_pager.ViewPagerAdapter
import com.google.android.material.tabs.TabLayoutMediator

class SearchFragment : BaseFragment<FragmentSearchBinding>() {

    override fun getViewBinding() = FragmentSearchBinding.inflate(layoutInflater)

    override fun getHasOptionsMenu() = false

    override fun getShowBottomNavigation() = true

    fun getTabs() = arrayOf(TAB_ITEM_NEWS, TAB_ITEM_FINANCES)

    override fun initView() {
        binding.apply {
            viewPager.adapter = ViewPagerAdapter(getTabs(), childFragmentManager, lifecycle)
            TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.text = getTabs()[position]
            }.attach()
        }
    }

    companion object {
        const val TAB_ITEM_NEWS = "tab_item_news"
        const val TAB_ITEM_FINANCES = "tab_item_finances"
    }

}