package com.example.updatedfragmentandnavigation.fragment

import androidx.core.os.bundleOf
import androidx.navigation.fragment.findNavController
import com.example.updatedfragmentandnavigation.R
import com.example.updatedfragmentandnavigation.base.BaseFragment
import com.example.updatedfragmentandnavigation.databinding.FragmentMoreBinding

class MoreFragment : BaseFragment<FragmentMoreBinding>() {
    override fun getViewBinding() = FragmentMoreBinding.inflate(layoutInflater)

    override fun getHasOptionsMenu() = false

    override fun getShowBottomNavigation() = true

    override fun initView() {
        binding.toCatalog.setOnClickListener{
            findNavController().navigate(
                R.id.action_moreFragment_to_navigation_catalog,
                bundleOf(
                    CatalogFragment.ARG_CLASS_NAME to this::class.java.simpleName
                )
            )
        }
    }

}