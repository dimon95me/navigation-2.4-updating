package com.example.updatedfragmentandnavigation.view_pager

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.updatedfragmentandnavigation.fragment.CatalogFragment
import com.example.updatedfragmentandnavigation.fragment.EmptyFragment
import com.example.updatedfragmentandnavigation.fragment.ProductDetailsFragment
import com.example.updatedfragmentandnavigation.fragment.SearchFragment

class ViewPagerAdapter(private val items: Array<String>, fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    override fun getItemCount() = items.size

    override fun createFragment(position: Int) = when (items[position]) {
        SearchFragment.TAB_ITEM_NEWS -> {
            CatalogFragment()
        }
        SearchFragment.TAB_ITEM_FINANCES -> {
            ProductDetailsFragment()
        }
        else -> SearchFragment()
    }

}