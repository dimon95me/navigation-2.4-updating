package com.example.updatedfragmentandnavigation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
import com.example.updatedfragmentandnavigation.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private var binding: ActivityMainBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        NavigationUI.setupWithNavController(binding!!.bottomNav, findNavController(R.id.main_fragment_nav))
    }

    override fun onSupportNavigateUp(): Boolean {
        findNavController(R.id.main_fragment_nav).navigateUp()
        return super.onSupportNavigateUp()
    }

    fun getBinding(): ActivityMainBinding? = binding

}
