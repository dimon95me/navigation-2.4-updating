package com.example.updatedfragmentandnavigation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.viewbinding.ViewBinding
import com.example.updatedfragmentandnavigation.MainActivity
import com.example.updatedfragmentandnavigation.R
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.io.IOException

abstract class BaseFragment<T : ViewBinding> : Fragment() {

    private var _binding: ViewBinding? = null

    protected val binding: T
        get() = _binding as T

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = getViewBinding()
        return requireNotNull(_binding).root
    }

    abstract fun getViewBinding(): T

    abstract fun initView()

    open fun getHasOptionsMenu() = true

    open fun getShowBottomNavigation() = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
            (requireActivity() as AppCompatActivity).supportActionBar?.title =
                this::class.java.simpleName
            (requireActivity() as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(
                getHasOptionsMenu()
            )
//            (requireActivity() as MainActivity).getBinding()?.bottomNav?.visibility =
//                if (getShowBottomNavigation()) View.VISIBLE else View.GONE

        initView()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}